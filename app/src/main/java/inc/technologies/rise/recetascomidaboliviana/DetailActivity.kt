package inc.technologies.rise.recetascomidaboliviana

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterIngrediente
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterInstruccion
import inc.technologies.rise.recetascomidaboliviana.database.RecetaBDHelper
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.activity_detail.*
import java.util.*

class DetailActivity : AppCompatActivity() {

    private var receta: Receta = Receta()
    private var lsIngredientes: ArrayList<String> = ArrayList()
    private var lsInstrucciones: ArrayList<String> = ArrayList()
    private lateinit var recetaDBHelper: RecetaBDHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        recetaDBHelper = RecetaBDHelper(this)

        // Get Object Receta from adapter.
        receta = intent?.extras?.getParcelable("Receta") as Receta

        // Carga de Datos.
        isFavorite()
        loadImagen()
        loadDatos()
        loadIngredientes()
        loadInstrucciones()

        //Listeners
        clickListener()

        toolbar.title = receta.nombre
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun clickListener() {
        btn_add_favorite_detail.setOnClickListener { addFavorite() }
    }

    private fun addFavorite() {
        if (recetaDBHelper.existeReceta(receta.id.toString())) {
            recetaDBHelper.deleteReceta(receta.id.toString())
            btn_add_favorite_detail.setImageResource(R.drawable.ic_favorite_grey)
//            Snackbar.make(coordinator, R.string.snackbar_msg_eliminado, Snackbar.LENGTH_SHORT).show()
        } else {
            recetaDBHelper.insertReceta(receta.id.toString())
            btn_add_favorite_detail.setImageResource(R.drawable.ic_favorite_red)
//            Snackbar.make(coordinator, R.string.snackbar_msg_add, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun isFavorite() {
        if (recetaDBHelper.existeReceta(receta.id.toString())) {
            btn_add_favorite_detail.setImageResource(R.drawable.ic_favorite_red)
        } else {
            btn_add_favorite_detail.setImageResource(R.drawable.ic_favorite_grey)
        }
    }

    private fun loadImagen() {
        Glide.with(this)
                .load(receta.img_receta)
                .crossFade()
                .centerCrop()
                .into(image_detail)
    }

    private fun loadDatos() {
        tv_detail_nombre_receta.text = receta.nombre
        tv_detail_descripcion_receta.text = receta.descripcion

        lsIngredientes = receta.ingredientes.split("@") as ArrayList<String>
        lsInstrucciones = receta.instrucciones.split("@") as ArrayList<String>

    }

    private fun loadIngredientes() {
        rv_ingredientes_receta.layoutManager = LinearLayoutManager(this)
        rv_ingredientes_receta.adapter = AdapterIngrediente(lsIngredientes, this)
        rv_ingredientes_receta.isNestedScrollingEnabled = false
    }

    private fun loadInstrucciones() {
        rv_instrucciones_receta.layoutManager = LinearLayoutManager(this)
        rv_instrucciones_receta.adapter = AdapterInstruccion(lsInstrucciones, this)
        rv_instrucciones_receta.isNestedScrollingEnabled = false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_share -> {
                val intent = Intent()
                intent.action = Intent.ACTION_SEND
                intent.putExtra(Intent.EXTRA_TEXT, "${resources.getString(R.string.texto_compartir_primero)} ${receta.nombre}. ${resources.getString(R.string.texto_compartir_segundo)} ${resources.getString(R.string.app_name)} ${resources.getString(R.string.texto_compartir_tercero_link)}")
                intent.type = "text/plain"
                startActivity(Intent.createChooser(intent, resources.getString(R.string.compartir_receta)))
            }
            else -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

}

package inc.technologies.rise.recetascomidaboliviana

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.firebase.database.DatabaseException
import com.google.firebase.database.FirebaseDatabase
import inc.technologies.rise.recetascomidaboliviana.fragments.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_main, FragmentRecetas.newInstance(), "Recetas")
                .commit()

        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        } catch (e: DatabaseException) {
            e.printStackTrace()
        }


        clickBottomBar()

    }

    private fun clickBottomBar() {
        bottomBar.setOnTabSelectListener {
            when (it) {
                R.id.tab_recetas -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_main, FragmentRecetas.newInstance(), "Recetas")
                            .commit()
                }

                R.id.tab_menu -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_main, FragmentMenu.newInstance(), "Menu")
                            .commit()
                }

                R.id.tab_departamentos -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_main, FragmentBolivia.newInstance(), "Bolivia")
                            .commit()
                }

                R.id.tab_buscar -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_main, FragmentBuscar.newInstance(), "Buscar")
                            .commit()
                }

                R.id.tab_favoritos -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_main, FragmentFavoritos.newInstance(), "Recetas")
                            .commit()
                }
            }
        }
    }

}

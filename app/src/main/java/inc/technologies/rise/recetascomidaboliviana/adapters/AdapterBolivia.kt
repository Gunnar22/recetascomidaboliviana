package inc.technologies.rise.recetascomidaboliviana.adapters

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import inc.technologies.rise.recetascomidaboliviana.MainActivity
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.fragments.FragmentDetailReceta
import inc.technologies.rise.recetascomidaboliviana.objects.Bolivia
import kotlinx.android.synthetic.main.item_bolivia_rv.view.*

class AdapterBolivia(private val items: ArrayList<Bolivia>, private val context: Context) : RecyclerView.Adapter<ViewHolderBolivia>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderBolivia {
        return ViewHolderBolivia(LayoutInflater.from(context).inflate(R.layout.item_bolivia_rv, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderBolivia, position: Int) {
        holder.nomDepBolivia.text = items[position].nombre

        Glide.with(context)
                .load(items[position].imgUrl)
                .crossFade()
                .centerCrop()
                .into(holder.imgDepBolivia)

        holder.itemView.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("Tipo", items[position].nombre)
            bundle.putString("orden", "2")
            val frag = FragmentDetailReceta.newInstance()
            frag.arguments = bundle

            (context as MainActivity).supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_main, frag, "DetailReceta").addToBackStack("Main")
                    .commit()

        }
    }
}

class ViewHolderBolivia(view: View) : RecyclerView.ViewHolder(view) {

    val imgDepBolivia = view.img_dep_bolivia
    val nomDepBolivia = view.nombre_dep_bolivia
}
package inc.technologies.rise.recetascomidaboliviana.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import inc.technologies.rise.recetascomidaboliviana.R
import kotlinx.android.synthetic.main.item_ingredientes_rv.view.*

class AdapterIngrediente(private val items: ArrayList<String>, private val context: Context): RecyclerView.Adapter<ViewHolderIngredientes>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderIngredientes {
        return ViewHolderIngredientes(LayoutInflater.from(context).inflate(R.layout.item_ingredientes_rv, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderIngredientes, position: Int) {
        holder.ingrediente.text = items[position]
    }

}

class ViewHolderIngredientes(view: View): RecyclerView.ViewHolder(view) {
    val ingrediente: TextView = view.tv_ingrediente_rv
}
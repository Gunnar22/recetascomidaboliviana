package inc.technologies.rise.recetascomidaboliviana.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import inc.technologies.rise.recetascomidaboliviana.R
import kotlinx.android.synthetic.main.item_instrucciones_rv.view.*

class AdapterInstruccion(private val items: ArrayList<String>, private val context: Context): RecyclerView.Adapter<ViewHolderInstruccion>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderInstruccion {
        return ViewHolderInstruccion(LayoutInflater.from(context).inflate(R.layout.item_instrucciones_rv, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderInstruccion, position: Int) {
        holder.tvNumero.text = (position + 1).toString()
        holder.tvInstruccion.text = items[position]
    }

}

class ViewHolderInstruccion(view: View): RecyclerView.ViewHolder(view){

    val tvNumero: TextView = view.tv_instrucciones_numero
    val tvInstruccion: TextView = view.tv_instruccion_receta
}
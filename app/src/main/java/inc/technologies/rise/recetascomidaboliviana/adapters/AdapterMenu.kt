package inc.technologies.rise.recetascomidaboliviana.adapters

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import inc.technologies.rise.recetascomidaboliviana.MainActivity
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.fragments.FragmentDetailReceta
import inc.technologies.rise.recetascomidaboliviana.objects.Menu
import kotlinx.android.synthetic.main.item_menu_rv.view.*

class AdapterMenu(private val items: ArrayList<Menu>, private val context: Context) : RecyclerView.Adapter<ViewHolderMenu>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMenu {
        return ViewHolderMenu(LayoutInflater.from(context).inflate(R.layout.item_menu_rv, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderMenu, position: Int) {
        holder.nombreMenu.text = items[position].nombre

        Glide.with(context)
                .load(items[position].imgUrl)
                .crossFade()
                .centerCrop()
                .into(holder.imgMenu)

        holder.itemView.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("Tipo", items[position].nombre)
            bundle.putString("orden", "1")
            val frag = FragmentDetailReceta.newInstance()
            frag.arguments = bundle

            (context as MainActivity).supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_main, frag, "DetailReceta").addToBackStack("Main")
                    .commit()

        }
    }

}

class ViewHolderMenu(view: View) : RecyclerView.ViewHolder(view) {
    val imgMenu = view.img_menu
    val nombreMenu = view.nombre_menu
}
package inc.technologies.rise.recetascomidaboliviana.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import inc.technologies.rise.recetascomidaboliviana.DetailActivity
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.database.RecetaBDHelper
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.item_receta_rv.view.*

class AdapterReceta(private val items: ArrayList<Receta>, private val context: Context) : RecyclerView.Adapter<ViewHolderReceta>() {

    private lateinit var recetaDBHelper: RecetaBDHelper

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderReceta {
        recetaDBHelper = RecetaBDHelper(context)

        return ViewHolderReceta(LayoutInflater.from(context).inflate(R.layout.item_receta_rv, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderReceta, position: Int) {
        holder.tvNombreReceta?.text = items[position].nombre

        Glide.with(context)
                .load(items[position].img_receta)
                .crossFade()
                .centerCrop()
                .into(holder.imageViewReceta)

        Glide.with(context)
                .load(items[position].img_tipo)
                .crossFade()
                .centerCrop()
                .into(holder.imageViewTipo)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("Receta", items[position])
            context.startActivity(intent)
        }

        holder.btnFavorite.setOnClickListener {
            if (recetaDBHelper.existeReceta(items[position].id.toString())) {
                holder.btnFavorite.setImageResource(R.drawable.ic_not_favorite)
                recetaDBHelper.deleteReceta(items[position].id.toString())
            } else {
                holder.btnFavorite.setImageResource(R.drawable.ic_add_favorite)
                recetaDBHelper.insertReceta(items[position].id.toString())
            }
        }

        if (recetaDBHelper.existeReceta(items[position].id.toString())) {
            holder.btnFavorite.setImageResource(R.drawable.ic_add_favorite)
        } else {
            holder.btnFavorite.setImageResource(R.drawable.ic_not_favorite)
        }

    }
}

class ViewHolderReceta(view: View) : RecyclerView.ViewHolder(view) {

    val imageViewReceta = view.img_receta
    val tvNombreReceta = view.nombre_receta_tv
    val imageViewTipo = view.img_tipo_receta
    val btnFavorite = view.btn_add_favorite
}
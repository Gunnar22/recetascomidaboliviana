package inc.technologies.rise.recetascomidaboliviana.database

import android.provider.BaseColumns

object DBContract {

    class RecetaEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "receta"
            val COLUMN_RECETA_ID = "recetaid"
        }
    }
}
package inc.technologies.rise.recetascomidaboliviana.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class RecetaBDHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    @Throws(SQLiteConstraintException::class)
    fun insertReceta(id: String): Boolean {
        val db = writableDatabase

        val values = ContentValues()
        values.put(DBContract.RecetaEntry.COLUMN_RECETA_ID, id)

        val newRowId = db.insert(DBContract.RecetaEntry.TABLE_NAME, null, values)
        Log.i(RecetaBDHelper::class.java.name, newRowId.toString())
        println("Se inserto la receta id: ${id}")

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteReceta(id: String): Boolean {
        val db = writableDatabase

        // Definir el 'where' parte del query.
        val selection = DBContract.RecetaEntry.COLUMN_RECETA_ID + " LIKE ?"

        // Espeficicando arguments en in placeholder order.
        val selectionArgs = arrayOf(id)

        db.delete(DBContract.RecetaEntry.TABLE_NAME, selection, selectionArgs)
        println("Se elimino la receta id: ${id}")

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun existeReceta(recetaid: String): Boolean {

        var receta = ""
        val db = writableDatabase
        var cursor: Cursor? = null

        try {
            cursor = db.rawQuery("select * from " + DBContract.RecetaEntry.TABLE_NAME + " WHERE " + DBContract.RecetaEntry.COLUMN_RECETA_ID + "='" + recetaid + "'", null)
        } catch (e: SQLiteException) {
            // Si la tabla no esta presente, crearlo
            db.execSQL(SQL_CREATE_ENTRIES)
            receta = "BD Created."
            println(receta)
        }

        if (cursor!!.moveToFirst()) {
            receta = "existe"
            println(receta)
        }

        return receta.equals("existe")
    }

    companion object {
        var DATABASE_VERSION = 1
        var DATABASE_NAME = "Receta.db"

        private val SQL_CREATE_ENTRIES =
                "CREATE TABLE " + DBContract.RecetaEntry.TABLE_NAME + " (" +
                        DBContract.RecetaEntry.COLUMN_RECETA_ID + " TEXT PRIMARY KEY)"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBContract.RecetaEntry.TABLE_NAME
    }

}
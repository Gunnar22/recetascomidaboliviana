package inc.technologies.rise.recetascomidaboliviana.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterBolivia
import inc.technologies.rise.recetascomidaboliviana.objects.Bolivia
import kotlinx.android.synthetic.main.bolivia_fragment.view.*

class FragmentBolivia : Fragment() {

    val departamentos: ArrayList<Bolivia> = ArrayList()
    var rvBolvia: RecyclerView? = null

    companion object {

        fun newInstance(): FragmentBolivia {
            return FragmentBolivia()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.bolivia_fragment, container, false)

        view.toolbar_bolivia.title = context?.getString(R.string.tab_pais_nombre)

        rvBolvia = view.findViewById<View>(R.id.rv_bolivia) as RecyclerView
        rvBolvia?.layoutManager = GridLayoutManager(context, 2)
        rvBolvia?.adapter = AdapterBolivia(departamentos, context!!)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadDatesToBolivia()
    }

    private fun loadDatesToBolivia() {
        departamentos.removeAll(departamentos)
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/beni_dep.jpg?alt=media&token=f199bf26-8b03-4628-969a-ca6d0c0275bd", getString(R.string.beni_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/cocha_dep.jpg?alt=media&token=416f2db4-7058-48b6-bf09-2e322637fe17", getString(R.string.cbba_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/la_paz_dep.jpg?alt=media&token=49a272c0-1e85-4216-8cfc-0873ea2c5d02", getString(R.string.la_paz_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/oruro_dep.jpg?alt=media&token=ba410c50-aaae-43cf-b460-86fad7e6d189", getString(R.string.oruro_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/pando_dep.jpg?alt=media&token=65dd7da4-03e7-4911-b265-e6cbd0a179b8", getString(R.string.pando_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/potosi_dep.jpg?alt=media&token=18264aaf-8a1f-444a-9afe-a21c951ef8c4", getString(R.string.potosi_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/sc_dep.jpg?alt=media&token=ec15f147-f176-42a9-9bc5-0ad04ebe8b4f", getString(R.string.sc_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/sucre_dep.jpg?alt=media&token=c0e700cf-bd58-43fd-a4bb-87dad6bba8b4", getString(R.string.sucre_dep)))
        departamentos.add(Bolivia("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/tarija_dep.jpg?alt=media&token=6ce3d558-92be-4425-9afd-2ba7cbb388fa", getString(R.string.tarija_dep)))
    }

}
package inc.technologies.rise.recetascomidaboliviana.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import com.miguelcatalan.materialsearchview.MaterialSearchView
import inc.technologies.rise.recetascomidaboliviana.DetailActivity
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.ViewHolderReceta
import inc.technologies.rise.recetascomidaboliviana.database.RecetaBDHelper
import inc.technologies.rise.recetascomidaboliviana.objects.FirebaseReference
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.buscar_fragment.view.*


class FragmentBuscar : Fragment() {

    private var rvBuscar: RecyclerView? = null
    private var databaseReference: DatabaseReference? = null

    private lateinit var recetaDBHelper: RecetaBDHelper

    companion object {

        fun newInstance(): FragmentBuscar {
            return FragmentBuscar()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.buscar_fragment, container, false)

        view.toolbar_search.title = context?.getString(R.string.search_menu)

        // Para que al crear un menu me reconosca a mi toolbar, osea poner el menu dentro de mi toolbar.
        (activity as AppCompatActivity).setSupportActionBar(view.toolbar_search)

        rvBuscar = view.findViewById<View>(R.id.rv_buscar) as RecyclerView
        rvBuscar?.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initFirebase()

        initSearchView()

    }

    /**
     * We use searchView from gitHub for our searches.
     */
    private fun initSearchView() {

        view?.search_view!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener, MaterialSearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                firebaseRecetaSearch(newText!!.toString().toLowerCase())
                return true
            }
        }
        )

    }

    /**
     * We init all Firebase tools that we'll need.
     */
    private fun initFirebase() {
        val database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference(FirebaseReference.FIREBASE_REFERENCE)

        recetaDBHelper = RecetaBDHelper(context!!)
    }

    /**
     * This functionality is for search foods from Firebase.
     */
    private fun firebaseRecetaSearch(searchText: String) {

        if (searchText.isEmpty()) {

            val adapterEmpty: FirebaseRecyclerAdapter<Receta, ViewHolderReceta>? = null
            rvBuscar?.adapter = adapterEmpty

        } else {

            val query: Query = databaseReference!!.orderByChild("nombre_key").startAt(searchText).endAt(searchText + "\uf8ff")

            val options = FirebaseRecyclerOptions.Builder<Receta>()
                    .setQuery(query, Receta::class.java)
                    .setLifecycleOwner(this)
                    .build()

            val adapter = object : FirebaseRecyclerAdapter<Receta, ViewHolderReceta>(options) {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderReceta {
                    return ViewHolderReceta(LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_receta_rv, parent, false))
                }

                override fun onBindViewHolder(holder: ViewHolderReceta, position: Int, model: Receta) {
                    holder.tvNombreReceta?.text = model.nombre

                    Glide.with(context)
                            .load(model.img_receta)
                            .crossFade()
                            .centerCrop()
                            .into(holder.imageViewReceta)

                    Glide.with(context)
                            .load(model.img_tipo)
                            .crossFade()
                            .centerCrop()
                            .into(holder.imageViewTipo)

                    holder.itemView.setOnClickListener {
                        val intent = Intent(context, DetailActivity::class.java)
                        intent.putExtra("Receta", model)
                        context!!.startActivity(intent)
                    }

                    holder.btnFavorite.setOnClickListener {
                        if (recetaDBHelper.existeReceta(model.id.toString())) {
                            holder.btnFavorite.setImageResource(R.drawable.ic_not_favorite)
                            recetaDBHelper.deleteReceta(model.id.toString())
                        } else {
                            holder.btnFavorite.setImageResource(R.drawable.ic_add_favorite)
                            recetaDBHelper.insertReceta(model.id.toString())
                        }
                    }

                    if (recetaDBHelper.existeReceta(model.id.toString())) {
                        holder.btnFavorite.setImageResource(R.drawable.ic_add_favorite)
                    } else {
                        holder.btnFavorite.setImageResource(R.drawable.ic_not_favorite)
                    }
                }
            }

            rvBuscar?.adapter = adapter

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_search, menu)
        val item: MenuItem = menu!!.findItem(R.id.action_search)
        view?.search_view?.setMenuItem(item)

        super.onCreateOptionsMenu(menu, inflater)
    }

}
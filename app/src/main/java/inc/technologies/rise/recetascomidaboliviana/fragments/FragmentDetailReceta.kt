package inc.technologies.rise.recetascomidaboliviana.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterReceta
import inc.technologies.rise.recetascomidaboliviana.objects.FirebaseReference
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.detail_menu_fragment.view.*

class FragmentDetailReceta : Fragment() {

    private val recetas: ArrayList<Receta> = ArrayList()
    private var databaseReference: DatabaseReference? = null

    var rvDetailMenu: RecyclerView? = null

    companion object {

        fun newInstance(): FragmentDetailReceta {
            return FragmentDetailReceta()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.detail_menu_fragment, container, false)

        initToolbar(view)
        initViews(view)

        view.toolbar_detail_menu.setNavigationOnClickListener { activity?.onBackPressed() }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDatesFromFirebase(arguments?.getString("Tipo")!!)
    }

    private fun initToolbar(view: View) {
        view.toolbar_detail_menu.title = arguments?.getString("Tipo")
        (activity as AppCompatActivity).setSupportActionBar(view.toolbar_detail_menu)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViews(view: View) {
        rvDetailMenu = view.findViewById<View>(R.id.rv_detail_menu) as RecyclerView
        rvDetailMenu?.layoutManager = LinearLayoutManager(context)
        rvDetailMenu?.adapter = AdapterReceta(recetas, context!!)
    }

    private fun getDatesFromFirebase(tipo: String) {

        val database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference(FirebaseReference.FIREBASE_REFERENCE)

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                recetas.removeAll(recetas)
                for (snapshot in dataSnapshot.children) {
                    val receta: Receta = snapshot.getValue(Receta::class.java)!!

                    if (arguments?.getString("orden").equals("1")) {
                        if (receta.tipo == tipo.toLowerCase()) recetas.add(receta)
                    } else {
                        if (receta.departamento == tipo) recetas.add(receta)
                    }

                }

                rvDetailMenu?.adapter?.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

    override fun onResume() {
        super.onResume()
        rvDetailMenu?.adapter?.notifyDataSetChanged()
    }

}
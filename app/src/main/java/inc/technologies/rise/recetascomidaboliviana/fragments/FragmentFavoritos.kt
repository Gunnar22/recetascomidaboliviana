package inc.technologies.rise.recetascomidaboliviana.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterReceta
import inc.technologies.rise.recetascomidaboliviana.database.RecetaBDHelper
import inc.technologies.rise.recetascomidaboliviana.objects.FirebaseReference
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.favoritos_fragment.view.*

class FragmentFavoritos : Fragment() {

    private val recetasFav: ArrayList<Receta> = ArrayList()
    private var rvFavoritos: RecyclerView? = null
    private lateinit var recetaDBHelper: RecetaBDHelper
    private var databaseReference: DatabaseReference? = null

    companion object {

        fun newInstance(): FragmentFavoritos {
            return FragmentFavoritos()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.favoritos_fragment, container, false)

        recetaDBHelper = RecetaBDHelper(activity!!)

        view.toolbar_favoritos.title = context?.getString(R.string.tab_favoritos_nombre)

        rvFavoritos = view.findViewById<View>(R.id.rv_favoritos) as RecyclerView
        rvFavoritos?.layoutManager = LinearLayoutManager(context)
        rvFavoritos?.adapter = AdapterReceta(recetasFav, context!!)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fillListFavoritos()
    }

    private fun fillListFavoritos() {
        val database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference(FirebaseReference.FIREBASE_REFERENCE)

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                recetasFav.removeAll(recetasFav)
                for (snapshot in dataSnapshot.children) {
                    val receta: Receta = snapshot.getValue(Receta::class.java)!!

                    if (recetaDBHelper.existeReceta(receta.id.toString())) {
                        recetasFav.add(receta)
                    }
                }

                rvFavoritos?.adapter?.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    override fun onResume() {
        super.onResume()
        fillListFavoritos()
//        rvFavoritos?.adapter?.notifyDataSetChanged()
    }
}
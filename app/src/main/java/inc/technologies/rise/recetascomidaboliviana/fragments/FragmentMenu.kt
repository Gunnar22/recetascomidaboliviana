package inc.technologies.rise.recetascomidaboliviana.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterMenu
import inc.technologies.rise.recetascomidaboliviana.objects.Menu
import kotlinx.android.synthetic.main.menu_fragment.view.*

class FragmentMenu : Fragment() {

    val menus: ArrayList<Menu> = ArrayList()
    var rvMenu: RecyclerView? = null

    companion object {

        fun newInstance(): FragmentMenu {
            return FragmentMenu()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.menu_fragment, container, false)

        view.toolbar_menu.title = context?.getString(R.string.tab_menu_nombre)

        rvMenu = view.findViewById<View>(R.id.rv_menu) as RecyclerView
        rvMenu?.layoutManager = GridLayoutManager(context, 2)
        rvMenu?.adapter = AdapterMenu(menus, context!!)

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadDatesToMenu()
    }

    private fun loadDatesToMenu() {
        menus.removeAll(menus)
        menus.add(Menu("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/entradas_img.png?alt=media&token=e50fd250-9015-4449-af68-5b6735516407", getString(R.string.entradas_menu)))
        menus.add(Menu("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/bebidas_img.png?alt=media&token=8e4e95be-cf00-4b5e-972f-34645835dac6", getString(R.string.bebidas_menu)))
        menus.add(Menu("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/sopa_img.png?alt=media&token=62199428-db04-46ff-baf5-68245b2ddf8a", getString(R.string.sopas_menu)))
        menus.add(Menu("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/postres_img.png?alt=media&token=13a0ef64-56e5-4da3-a5bf-96d122e30530", getString(R.string.postres_menu)))
        menus.add(Menu("https://firebasestorage.googleapis.com/v0/b/recetas-comida-boliviana-97434.appspot.com/o/plato_fuerte_img.png?alt=media&token=0450276c-3acc-4bf6-8760-6bd3d664a5eb", getString(R.string.plato_fuerte_menu)))
    }

}
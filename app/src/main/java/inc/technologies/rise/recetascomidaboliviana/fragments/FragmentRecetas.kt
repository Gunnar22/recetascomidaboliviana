package inc.technologies.rise.recetascomidaboliviana.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import inc.technologies.rise.recetascomidaboliviana.R
import inc.technologies.rise.recetascomidaboliviana.adapters.AdapterReceta
import inc.technologies.rise.recetascomidaboliviana.objects.FirebaseReference
import inc.technologies.rise.recetascomidaboliviana.objects.Receta
import kotlinx.android.synthetic.main.recetas_fragment.view.*

class FragmentRecetas : Fragment() {

    private val recetas: ArrayList<Receta> = ArrayList()
    private var databaseReference: DatabaseReference? = null

    var rvRecetas: RecyclerView? = null

    companion object {

        fun newInstance(): FragmentRecetas {
            return FragmentRecetas()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.recetas_fragment, container, false)

        view.toolbar_recetas.title = context?.getString(R.string.tab_receta_nombre)

        rvRecetas = view.findViewById<View>(R.id.rv_recetas) as RecyclerView
        rvRecetas?.layoutManager = LinearLayoutManager(context)
        rvRecetas?.adapter = AdapterReceta(recetas, context!!)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getDatesFromFirebase()
    }

    private fun getDatesFromFirebase() {

        val database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference(FirebaseReference.FIREBASE_REFERENCE)

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                recetas.removeAll(recetas)
                for (snapshot in dataSnapshot.children) {
                    val receta: Receta = snapshot.getValue(Receta::class.java)!!
                    recetas.add(receta)
                }

                rvRecetas?.adapter?.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

    override fun onResume() {
        super.onResume()
        rvRecetas?.adapter?.notifyDataSetChanged()
    }

}
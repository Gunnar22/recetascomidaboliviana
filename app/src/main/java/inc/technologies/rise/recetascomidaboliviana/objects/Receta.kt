package inc.technologies.rise.recetascomidaboliviana.objects

import android.os.Parcel
import android.os.Parcelable

data class Receta(val id: Int = -1, var nombre: String = "", var nombreKey: String = "", var descripcion: String = "", var ingredientes: String = "", var instrucciones: String = "", var img_receta: String = "", val departamento: String = "", val tipo: String = "", var img_tipo: String = "", val orden: Int = -1) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(nombre)
        parcel.writeString(nombreKey)
        parcel.writeString(descripcion)
        parcel.writeString(ingredientes)
        parcel.writeString(instrucciones)
        parcel.writeString(img_receta)
        parcel.writeString(departamento)
        parcel.writeString(tipo)
        parcel.writeString(img_tipo)
        parcel.writeInt(orden)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Receta> {
        override fun createFromParcel(parcel: Parcel): Receta {
            return Receta(parcel)
        }

        override fun newArray(size: Int): Array<Receta?> {
            return arrayOfNulls(size)
        }
    }
}